/*****************************************************************************
 * GTK+ real-time plotting widget
 * Copyright (C) 2017 Rajan Gill
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#ifndef __RPLOT_3D_H__
#define __RPLOT_3D_H__

#include <gtk/gtk.h>
#include <stdint.h>
#include <vectorf.h>

/*****************************************************************************
 * Standard GObject macros 
 ****************************************************************************/
G_BEGIN_DECLS

#define RPLOT_TYPE_3D (rplot_3d_get_type())
G_DECLARE_FINAL_TYPE(Rplot3d, rplot_3d, RPLOT, 3D, GtkGLArea)

/*****************************************************************************
 * Public Types
 ****************************************************************************/

typedef enum {
	RPLOT_3D_NORMAL, /* sets up a std camera with std fov, no additional args */
	RPLOT_3D_ORTHOGONAL = 6,    /* sets up an orthogonal projection perspective, 
	pass in doubles l, r, b, t, n, f in camera frame */
	RPLOT_3D_FOLLOW, 				/* follows the 0th vehicle, using vector camera_pos */
	RPLOT_3D_FOLLOW_HEADING, /* fixed camera position, but rotates to keep vehicle centered */
} Rplot3dCameraType_e;

/*****************************************************************************
 * Public API Declarations
 ****************************************************************************/

/**
 * Instance initializer.
 * @rgba: array of array of 4 floats
 * Only call from main GTK thread.
 * @camera_pos: camera pos in world frame,
 * @camera_up : camera up vector in world frame,
 * @type: see above
 */

GtkWidget* 
rplot_3d_new(guint8 no_vehicles, const float (*rgbav)[4], 
						 guint8 no_lines, const float (*rgbal)[4],
						 guint8 no_ellipsoids, const float (*rgbae)[4],
						 vec3f  camera_pos, vec3f camera_up,
						 Rplot3dCameraType_e type, ...);

/**
 * Update line index
 * @a: start point
 * @b: end point
 */
void 
rplot_3d_update_lines(Rplot3d *p, guint8 index, const vec3f *a, const vec3f *b);

/**
 * Update vehicle # index
 * @pos: obvious
 * @q: this is the forward frame rotation. I.e., from world to vehicle frame.
 */
void
rplot_3d_update_vehicle(Rplot3d *p, guint8 index, const vec3f *pos, const rotf *q);

/**
 * Update ellisooid number index. 
 * The ellisoid is represented as the output of the unit ball under 
 * affine mapping @P*(ball) + @center
 * @P: can be NULL to only update center.
 */
void
rplot_3d_update_ellispoid(Rplot3d *p, guint8 index, const vec3f *center, const mat3f *P);



G_END_DECLS
#endif /* include guard */