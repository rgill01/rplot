/*****************************************************************************
 * GTK+ real-time plotting widget
 * Copyright (C) 2017 Rajan Gill
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#ifndef __RPLOT_2D_H__
#define __RPLOT_2D_H__

#include <gtk/gtk.h>
#include <stdint.h>

/*****************************************************************************
 * Standard GObject macros 
 ****************************************************************************/
G_BEGIN_DECLS

#define RPLOT_TYPE_2D (rplot_2d_get_type())
G_DECLARE_FINAL_TYPE(Rplot2d, rplot_2d, RPLOT, 2D, GtkGLArea)

/*****************************************************************************
 * Public Types
 ****************************************************************************/

/*****************************************************************************
 * Public API Declarations
 ****************************************************************************/

/**
 * Instance initializer. 
 * @n_series: number of series to plot.
 * @labels: array of strings 
 * @t_hist: history of data to display
 * @ymin, ymax: init y limits
 * @freqsample: the sampling frequency in [Hz]
 * @downsample: factor to down sample by. ex freqsample = 1khz, downsample = 2 
 *   -> show every other point.  
 * Only call from main GTK thread.
 */

GtkWidget* 
rplot_2d_new(guint8 n_series, 
						 const char **labels,
						 float t_hist, 
						 float ymin, 
						 float ymax, 
						 uint16_t freqsample,
						 uint16_t downsample);

/**
 * Add new data to the plot. This is fast, doesn't do any drawing.
 * @p:
 * @x: new x data.
 * @y: array of new y data whose length must be less than or equal to n_series.
 *
 * Treats the internal x,y buffers as circular.
 * Automatically adjusts y limits if new data exceeds current limits.
 *
 * Only call from main GTK thread
 */

void 
rplot_2d_push_data(Rplot2d *p, float t, const float *y);


G_END_DECLS
#endif /* include guard */