# rplot
GTK+ realtime plotting widget (2d and 3d) using <s>Cairo</s> OpenGL.


`./ex/ex2.out`

![ex2](ex2.png)

`./ex/ex4.out`

![ex4](ex4.png)

# dependencies
- clib
- GTK


# todo
- This readme. 
- Clean up ex code. (domain, etc.)
