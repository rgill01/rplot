/*****************************************************************************
 * GTK+ real-time plotting widget
 * Copyright (C) 2017 Rajan Gill
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#include <rplot2d.h>
#include "rplot2d_private.h"

/*****************************************************************************
 * Standard GObject macros
 ****************************************************************************/

G_DEFINE_TYPE(Rplot2d, rplot_2d, GTK_TYPE_GL_AREA)

/*****************************************************************************
 * Private API Declarations
 ****************************************************************************/

static void rplot_2d_realize(GtkWidget *);
static void rplot_2d_unrealize(GtkWidget *);
static void rplot_2d_destroy(GtkWidget *);
static void rplot_2d_resize(GtkGLArea *, int, int);
static gboolean rplot_2d_render(GtkGLArea *, GdkGLContext *);

/*****************************************************************************
 * Private API Definitions - Class methods
 ****************************************************************************/

static void rplot_2d_class_init(Rplot2dClass *klass)
{
	GtkWidgetClass  *w_class  = GTK_WIDGET_CLASS(klass);
	GtkGLAreaClass  *gl_class = GTK_GL_AREA_CLASS(klass);

	/* override functions */

	w_class->destroy 							= rplot_2d_destroy;
	w_class->realize 							= rplot_2d_realize;
	w_class->unrealize 						= rplot_2d_unrealize;
	gl_class->render  	 					= rplot_2d_render;
	gl_class->resize 							= rplot_2d_resize;
}

static void rplot_2d_init(Rplot2d *p)
{
	mat4f_copy(&mat4f_identity, &p->T);
	p->bufi = 0;
	p->bufhead = 0;
}


gboolean rplot_2d_render(GtkGLArea *glarea, GdkGLContext *context)
{
	rplot_2d_draw(RPLOT_2D(glarea));

	return FALSE;
}

static void rplot_2d_resize(GtkGLArea *glarea, int w, int h)
{
	rplot_2d_render_background(RPLOT_2D(glarea));

	GTK_GL_AREA_CLASS(rplot_2d_parent_class)->resize(glarea, w, h);
}

static void rplot_2d_unrealize(GtkWidget *widget)
{
	rplot_2d_draw_deinit(RPLOT_2D(widget));

	GTK_WIDGET_CLASS(rplot_2d_parent_class)->unrealize(widget);
}

static void rplot_2d_realize(GtkWidget *widget)
{
	GTK_WIDGET_CLASS(rplot_2d_parent_class)->realize(widget);

	gtk_gl_area_make_current(GTK_GL_AREA(widget));

	if (gtk_gl_area_get_error(GTK_GL_AREA(widget)) != NULL)
		return;

	// gtk_gl_area_set_has_depth_buffer(GTK_GL_AREA(widget), GL_TRUE);
	// gtk_gl_area_set_auto_render(GTK_GL_AREA(widget), GL_TRUE);
	// gtk_gl_area_set_has_alpha(GTK_GL_AREA(widget), GL_TRUE);

  /* start GLEW extension handler */
#ifdef USE_GLEW
  glewExperimental = GL_TRUE;
  if (glewInit()) 
      fprintf(stderr, "Failed to initialize GLEW.\n");
#endif

  /* get version info */

  const GLubyte* renderer = glGetString(GL_RENDERER); // get renderer string
  const GLubyte* version = glGetString(GL_VERSION); // version as a string
  printf("Renderer: %s\n", renderer);
  printf("OpenGL version supported %s\n", version);

  /* init */

  rplot_2d_draw_init(RPLOT_2D(widget));
  rplot_2d_render_background(RPLOT_2D(widget));

  /* frame clock */

  GdkGLContext *glcontext = gtk_gl_area_get_context(GTK_GL_AREA(widget));
  GdkWindow *glwindow = gdk_gl_context_get_window(glcontext);
	GdkFrameClock *frame_clock = gdk_window_get_frame_clock(glwindow);

	// Connect update signal:
	g_signal_connect_swapped
		( frame_clock
		, "update"
		, G_CALLBACK(gtk_gl_area_queue_render)
		, widget
		) ;

	// Start updating:
	gdk_frame_clock_begin_updating(frame_clock);
	// gdk_threads_add_idle(gtk_gl_area_queue_render, widget);

	/* take up any free vertical space */
	gtk_widget_set_hexpand(widget, TRUE);
	gtk_widget_set_vexpand(widget, TRUE);

	// gtk_gl_area_set_auto_render(GTK_GL_AREA(widget), TRUE);
	// gtk_gl_area_set_has_alpha(GTK_GL_AREA(widget), TRUE);
}

static void rplot_2d_destroy(GtkWidget *widget)
{
	Rplot2d *p = RPLOT_2D(widget);

	g_free(p->s);
	p->s = NULL;
	p->n_series = 0;

	GTK_WIDGET_CLASS(rplot_2d_parent_class)->destroy(widget);
}

/*****************************************************************************
 * Public API Definitions
 ****************************************************************************/

GtkWidget* rplot_2d_new(guint8 n_series, 
						 const char **labels,
						 float t_hist, 
						 float ymin, 
						 float ymax, 
						 uint16_t freq,
						 uint16_t downsample)
{
	GtkWidget *widget = g_object_new(RPLOT_TYPE_2D, NULL);
	Rplot2d *p = RPLOT_2D(widget);

	/* allocate series */

	g_assert(n_series > 0);

  p->s 				= g_malloc0(sizeof(Rplot2dSeries) * n_series);
	for (int i = 0; i < n_series; i++) {
		p->s[i].pointsize = 2;
		if (labels != NULL)
			p->s[i].label = labels[i];
		else
			p->s[i].label = "";

		/* auto set rgba values based on n */

		static const float rgb[11][3] = 
			{ {230,25,75}, 
			  {60,180,75}, 
			  {255,225,25}, 
			  {0,130,200}, 
			  {245,130,48}, 
			  {70,240,240}, 
			  {240,50,230},
			  {250,190,190},
			  {170,110,40},
			  {170,255,195},
			  {128,128,128},};

		p->s[i].rgba[0] 	= rgb[i%11][0]/255;
		p->s[i].rgba[1]   = rgb[i%11][1]/255;
		p->s[i].rgba[2] 	= rgb[i%11][2]/255;
		p->s[i].rgba[3]   = 1;
	}
  
  p->n_series = n_series; 
  p->bufiskip = downsample;
  p->xhist   = t_hist;
  p->ymin    = ymin;
  p->ymax    = ymax;
  p->T[0][0] = 2 / t_hist;
  p->T[1][1] = 2/(p->ymax - p->ymin);
	p->T[1][3] = -(p->ymax + p->ymin)/(p->ymax - p->ymin);
  p->buflen  = t_hist * freq / downsample;
  
	return widget;
}

void rplot_2d_push_data(Rplot2d *p, float t, const float *ys)
{
	gboolean reset = FALSE;
	uint8_t i;

	if (++p->bufi >= p->bufiskip) {
		p->bufi = 0;	

		// gtk_gl_area_make_current(GTK_GL_AREA(p));

		for (i=0; i<p->n_series; i++) {
			GLfloat y = ys[i];

			if (y < p->ymin) {
				p->ymin = y*1.2;
				reset = TRUE;
			} 
			else if (y > p->ymax) {
				p->ymax = y*1.2;
				reset = TRUE;
			}
			glBindBuffer(GL_ARRAY_BUFFER, p->s[i].vbo_y);
			glBufferSubData(GL_ARRAY_BUFFER, sizeof(GLfloat)*p->bufhead, sizeof(GLfloat), &y);
		}

		glBindBuffer(GL_ARRAY_BUFFER, p->vbo_x);
		glBufferSubData(GL_ARRAY_BUFFER, sizeof(GLfloat)*p->bufhead, sizeof(GLfloat), &t);

		p->T[0][3] = 1 - 2*t/p->xhist;
		
		if (reset) {
			p->T[1][1] = 2/(p->ymax - p->ymin);
			p->T[1][3] = -(p->ymax + p->ymin)/(p->ymax - p->ymin);

			rplot_2d_render_background(p);
		}

		if (++p->bufhead >= p->buflen) 
			p->bufhead = 0;
	}
}