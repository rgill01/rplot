/*****************************************************************************
 * GTK+ real-time plotting widget
 * Copyright (C) 2017 Rajan Gill
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#include "rplot2d_private.h"
#include <string.h>

/*****************************************************************************
 * Preprocessor Definitions
 ****************************************************************************/		

#define MARGIN       10 // font margin to window 

/*****************************************************************************
 * Private API Definitions
 ****************************************************************************/

/*****************************************************************************
 * Public API Definitions
 ****************************************************************************/

void rplot_2d_draw_init(Rplot2d *p)
{
  /* vertex buffers */

  glGenBuffers(1, &p->vbo_x);  		/* 1d time data */
  glBindBuffer(GL_ARRAY_BUFFER, p->vbo_x);
  glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*p->buflen, NULL, GL_STREAM_DRAW);

  for (int i = 0; i < p->n_series; i++) {
  	glGenBuffers(1, &p->s[i].vbo_y);
  	glBindBuffer(GL_ARRAY_BUFFER, p->s[i].vbo_y);
  	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*p->buflen, NULL, GL_STREAM_DRAW);

  	glGenVertexArrays(1, &p->s[i].vao);
  	glBindVertexArray(p->s[i].vao);

		glBindBuffer(GL_ARRAY_BUFFER, p->vbo_x);
		glVertexAttribPointer(0, 1, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ARRAY_BUFFER, p->s[i].vbo_y);
  	glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(1);
  } 

  /* text */

  p->txt = glutil_text_initiate(FONT_SIZE);

  /* shader program */

  p->shader  = glutil_shaders_load(__DIR__ "/src/shaders/vs2d.glsl", __DIR__ "/src/shaders/fs2d.glsl");
  p->mvpID   = glGetUniformLocation(p->shader, "MVP");
  p->colourID = glGetUniformLocation(p->shader, "rgba");
  glClearColor(0, 0, 0, 1);

  /* backgorund */

  // p->b_shader = glutil_shaders_load(__DIR__ "/src/shade/b_vs2d.glsl", __DIR__ "/src/shaders/b_fs2d.glsl");
  glGenBuffers(1, &p->b_vbo);
  glBindBuffer(GL_ARRAY_BUFFER, p->b_vbo);
  float grid[] = { -1, 0,
                    1, 0 };
  glBufferData(GL_ARRAY_BUFFER, sizeof(grid), grid, GL_STATIC_DRAW);
  
  glGenVertexArrays(1, &p->b_vao);
  glBindVertexArray(p->b_vao);
  glVertexAttribPointer(0, 1, GL_FLOAT, GL_FALSE, sizeof(float)*2, NULL);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, sizeof(float)*2, (void *) sizeof(float));
  glEnableVertexAttribArray(1);
}

void rplot_2d_draw_deinit(Rplot2d *p)
{
  glDeleteProgram(p->shader);
  glDeleteBuffers(1, &p->vbo_x);
  glutil_text_destroy(p->txt);

  glDeleteBuffers(1, &p->b_vbo);
  glDeleteVertexArrays(1, &p->b_vao);

  for (int i=0; i<p->n_series; i++) {
    glDeleteBuffers(1, &p->s[i].vbo_y);
    glDeleteVertexArrays(1, &p->s[i].vao);
  }
}

void rplot_2d_render_background(Rplot2d *p)
{
  int width = gtk_widget_get_allocated_width(GTK_WIDGET(p));
  int height = gtk_widget_get_allocated_height(GTK_WIDGET(p));

  /* get strings */

  glutil_text_data texts[4 + p->n_series];

  for (int i = 0; i < p->n_series; i++) {
    texts[i].r    = p->s[i].rgba[0] * 255;
    texts[i].g    = p->s[i].rgba[1] * 255;
    texts[i].b    = p->s[i].rgba[2] * 255;

    texts[i].text = p->s[i].label;

    texts[i].px = MARGIN;
    texts[i].py = MARGIN + FONT_SIZE*(i+1);
  }

  texts[p->n_series].text = g_strdup_printf("t=-%.0f", p->xhist);
  texts[p->n_series].px   = MARGIN;
  texts[p->n_series].py   = height - MARGIN;

  texts[p->n_series+1].text = g_strdup_printf("%.1f", p->ymin);
  texts[p->n_series+1].px   = width - MARGIN - strlen(texts[p->n_series+1].text)*FONT_SIZE*0.5;  
  texts[p->n_series+1].py   = height - MARGIN;

  texts[p->n_series+2].text = g_strdup_printf("%.1f", (p->ymax + p->ymin)/2);
  texts[p->n_series+2].px   = width - MARGIN - strlen(texts[p->n_series+2].text)*FONT_SIZE*0.5;
  texts[p->n_series+2].py   = height/2.f;

  texts[p->n_series+3].text = g_strdup_printf("%.1f", p->ymax);
  texts[p->n_series+3].px   = width - MARGIN - strlen(texts[p->n_series+3].text)*FONT_SIZE*0.5;
  texts[p->n_series+3].py   = MARGIN + FONT_SIZE;

  for (int i = p->n_series; i < 4 + p->n_series; i ++) {
    texts[i].r = texts[i].g = texts[i].b = 255;
  }

	/* render text */

  glutil_text_render(p->txt, texts, 4 + p->n_series, width, height);

  /* clean up */

  for (int i = p->n_series; i < 4 + p->n_series; i ++) {
    g_free( (void *) texts[i].text);
  }
}

void rplot_2d_draw_background(Rplot2d *p)
{
  // glUseProgram(p->b_shader);
  glBindVertexArray(p->b_vao);
  glUniformMatrix4fv(p->mvpID, 1, GL_TRUE, &mat4f_identity[0][0]);
  glUniform4f(p->colourID, 0.5, 0.5, 0.5, 0.5);
  glDrawArrays(GL_LINES, 0, 2);
  
  glutil_text_draw(p->txt);
}

void rplot_2d_draw(Rplot2d *p)
{
  glClear(GL_COLOR_BUFFER_BIT);
  glEnable(GL_BLEND);

  /* data */
  glUseProgram(p->shader);
  glUniformMatrix4fv(p->mvpID, 1, GL_TRUE, &p->T[0][0]);

  for (int i=0; i < p->n_series; i++) {
    glPointSize(p->s[i].pointsize);
    glUniform4fv(p->colourID, 1, p->s[i].rgba);

    glBindVertexArray(p->s[i].vao);

    glDrawArrays(GL_POINTS, 0, p->buflen);

#if 0
    glLineWidth(3);
    long headP1 = (p->bufhead == GLBUF_LEN-1) ? 0 : p->bufhead+1;
    glDrawArrays(GL_LINES, headP1, GLBUF_LEN - headP1);
    glDrawArrays(GL_LINES, 0, p->bufhead);
#endif    
  }

  /* background */
  rplot_2d_draw_background(p);
}