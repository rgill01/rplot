/*****************************************************************************
 * GTK+ real-time plotting widget
 * Copyright (C) 2017 Rajan Gill
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#ifndef __RPLOT_2D_PRIVATE_H__
#define __RPLOT_2D_PRIVATE_H__

#include <rplot2d.h>
#include <vectorf.h>
#include <glutilities.h>

/*****************************************************************************
 * Preprocessor Definitions
 ****************************************************************************/

#define FONT_SIZE					14

/*****************************************************************************
 * Private Types
 ****************************************************************************/

typedef struct _Rplot2dSeries
{
	GLuint 			vbo_y; 			 /* gpu y point buffers */
	GLuint 			vao;

	/* aesthetics */

	float 			rgba[4];
	const char	*label;  				/* label used on legend */
	uint8_t			pointsize;
} Rplot2dSeries;


struct _Rplot2d
{
	GtkGLArea 		parent;

	/* private */

	Rplot2dSeries *s;
	uint8_t				n_series;
	
	uint32_t 			buflen,
		  					bufhead,				/* head of the circular x,y buffers */
	 							bufi,
  			  			bufiskip;       /* skip these many samples */

	float 				xhist,					/* last xlim data to display */
				 				ymax, ymin;	
	
	mat4f 				T;
	GLuint 				vbo_x;
	glutil_text 	*txt;
	GLuint 				shader, mvpID, colourID; 
	GLuint 				b_vbo, b_vao, b_shader;  /* background */
	/* I tried sharing the shaders, but ex3 looks noticably more choppy.. */
};

/*****************************************************************************
 * Public API Declarations
 ****************************************************************************/

void rplot_2d_draw_init(Rplot2d *p);
void rplot_2d_draw_deinit(Rplot2d *p);
void rplot_2d_render_background(Rplot2d *p);
void rplot_2d_draw(Rplot2d *p);

#endif /* include guard */