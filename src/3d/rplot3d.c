/*****************************************************************************
 * GTK+ real-time plotting widget
 * Copyright (C) 2017 Rajan Gill
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#include <rplot3d.h>
#include "rplot3d_private.h"
#include <string.h>
#include <stdarg.h>

/*****************************************************************************
 * Standard GObject macros
 ****************************************************************************/

G_DEFINE_TYPE(Rplot3d, rplot_3d, GTK_TYPE_GL_AREA)

/*****************************************************************************
 * Private API Declarations
 ****************************************************************************/

static void rplot_3d_realize(GtkWidget *);
static void rplot_3d_unrealize(GtkWidget *);
static void rplot_3d_destroy(GtkWidget *);
static void rplot_3d_resize(GtkGLArea *, int, int);
static gboolean rplot_3d_render(GtkGLArea *, GdkGLContext *);

static int initialize_transforms(mat4f **Ts, int n);
static void deinitialize_transforms(mat4f **T);

/*****************************************************************************
 * Private API Definitions - Class methods
 ****************************************************************************/

static void rplot_3d_class_init(Rplot3dClass *klass)
{
	GtkWidgetClass  *w_class  = GTK_WIDGET_CLASS(klass);
	GtkGLAreaClass  *gl_class = GTK_GL_AREA_CLASS(klass);

	/* override functions */

	w_class->destroy 							= rplot_3d_destroy;
	w_class->realize 							= rplot_3d_realize;
	w_class->unrealize 						= rplot_3d_unrealize;
	gl_class->render  	 					= rplot_3d_render;
	gl_class->resize 							= rplot_3d_resize;
}

static void rplot_3d_init(Rplot3d *p)
{
	mat4f_copy(&mat4f_identity, &p->Tworld2camera);
}


gboolean rplot_3d_render(GtkGLArea *glarea, GdkGLContext *context)
{
	rplot_3d_draw(RPLOT_3D(glarea));

	return FALSE;
}

static void rplot_3d_resize(GtkGLArea *glarea, int w, int h)
{
	Rplot3d *p = RPLOT_3D(glarea);
	if (p->type != RPLOT_3D_ORTHOGONAL) {
		mat4f_perspective(radiansf(45), (float) w/h, 0.1, 100, &p->perspective);
		mat4f_mult(&p->perspective, &p->Tworld2camera, &p->Tworld2cameraProj);
	}
	GTK_GL_AREA_CLASS(rplot_3d_parent_class)->resize(glarea, w, h);
}

static void rplot_3d_unrealize(GtkWidget *widget)
{
	rplot_3d_draw_deinit(RPLOT_3D(widget));

	GTK_WIDGET_CLASS(rplot_3d_parent_class)->unrealize(widget);
}

static void rplot_3d_realize(GtkWidget *widget)
{
	GTK_WIDGET_CLASS(rplot_3d_parent_class)->realize(widget);

	gtk_gl_area_make_current(GTK_GL_AREA(widget));

	if (gtk_gl_area_get_error(GTK_GL_AREA(widget)) != NULL)
		return;

	// gtk_gl_area_set_has_depth_buffer(GTK_GL_AREA(widget), GL_TRUE);
	// gtk_gl_area_set_auto_render(GTK_GL_AREA(widget), GL_TRUE);
	// gtk_gl_area_set_has_alpha(GTK_GL_AREA(widget), GL_TRUE);

  /* start GLEW extension handler */
#ifdef USE_GLEW
  glewExperimental = GL_TRUE;
  if (glewInit()) 
      fprintf(stderr, "Failed to initialize GLEW.\n");
#endif

  /* get version info */

  const GLubyte* renderer = glGetString(GL_RENDERER); // get renderer string
  const GLubyte* version = glGetString(GL_VERSION); // version as a string
  printf("Renderer: %s\n", renderer);
  printf("OpenGL version supported %s\n", version);

  /* init */
  rplot_3d_draw_init(RPLOT_3D(widget));

  /* frame clock */
  GdkGLContext *glcontext = gtk_gl_area_get_context(GTK_GL_AREA(widget));
  GdkWindow *glwindow = gdk_gl_context_get_window(glcontext);
	GdkFrameClock *frame_clock = gdk_window_get_frame_clock(glwindow);

	// Connect update signal:
	g_signal_connect_swapped
		( frame_clock
		, "update"
		, G_CALLBACK(gtk_gl_area_queue_render)
		, widget
		) ;

	// Start updating:
	gdk_frame_clock_begin_updating(frame_clock);
	// gdk_threads_add_idle(gtk_gl_area_queue_render, widget);

	/* take up any free vertical space */
	gtk_widget_set_hexpand(widget, TRUE);
	gtk_widget_set_vexpand(widget, TRUE);

	// gtk_gl_area_set_auto_render(GTK_GL_AREA(widget), TRUE);
	// gtk_gl_area_set_has_alpha(GTK_GL_AREA(widget), TRUE);
}

static void rplot_3d_destroy(GtkWidget *widget)
{
	Rplot3d *p = RPLOT_3D(widget);

	g_free(p->vehicles.rgba);
	g_free(p->lines.rgba);
	g_free(p->ellipses.rgba);

	deinitialize_transforms(&p->vehicles.Tmodel2world);
	deinitialize_transforms(&p->ellipses.Tmodel2world);

	p->vehicles.rgba = NULL;
	p->lines.rgba    = NULL;
	p->ellipses.rgba = NULL;

	p->vehicles.n = 0;
	p->lines.n    = 0;
	p->ellipses.n = 0;

	GTK_WIDGET_CLASS(rplot_3d_parent_class)->destroy(widget);
}

static int initialize_transforms(mat4f **Ts, int n)
{
  if ( (*Ts = g_malloc0(sizeof(mat4f)*n)) == NULL){
    fprintf(stderr, "malloc failed\n");
    return -1;
  }
  for (int i = 0; i < n; i ++) {
    memcpy((*Ts)[i], &mat4f_identity, sizeof(mat4f));
  }
  return 0;
}

static void deinitialize_transforms(mat4f **T)
{
  g_free(*T);
  (*T) = NULL;
}

/*****************************************************************************
 * Public API Definitions
 ****************************************************************************/

GtkWidget* rplot_3d_new(
		guint8 no_vehicles, const float (*rgbav)[4], 
		guint8 no_lines, const float (*rgbal)[4],
		guint8 no_ellipsoids, const float (*rgbae)[4],
		vec3f  camera_pos, vec3f camera_up,
		Rplot3dCameraType_e type, ...)
{
	GtkWidget *widget = g_object_new(RPLOT_TYPE_3D, NULL);
	Rplot3d *p = RPLOT_3D(widget);

	/* vehicles */

	if ( (p->vehicles.n = no_vehicles) > 0) {
		if ( (p->vehicles.rgba = g_malloc0(sizeof(float)*4*no_vehicles)) == NULL) {
			fprintf(stderr, "gmalloc0 failed\n");
			goto errout;
		}
		memcpy(p->vehicles.rgba, rgbav, sizeof(float)*4*no_vehicles);

		initialize_transforms(&p->vehicles.Tmodel2world, p->vehicles.n);
	}
	
	/* lines */

	if ( (p->lines.n = no_lines) > 0) {
		if ( (p->lines.rgba = g_malloc0(sizeof(float)*4*no_lines)) == NULL) {
			fprintf(stderr, "gmalloc0 failed\n");
			goto errout;
		}
		memcpy(p->lines.rgba, rgbal, sizeof(float)*4*no_lines);
	}

	/* ellipses */

	if ( (p->ellipses.n = no_ellipsoids) > 0) {
		if ( (p->ellipses.rgba = g_malloc0(sizeof(float)*4*no_ellipsoids)) == NULL) {
			fprintf(stderr, "gmalloc0 failed\n");
			goto errout;
		}
		memcpy(p->ellipses.rgba, rgbae, sizeof(float)*4*no_ellipsoids);

		initialize_transforms(&p->ellipses.Tmodel2world, p->ellipses.n);
	}

	/* camera releated */
	mat4f_look_at((const vec3f *)camera_pos, 
		&vec3f_zero, (const vec3f *)camera_up, &p->Tworld2camera);
	
	p->type = type;
	va_list valist;
	float orth_proj[6];
	switch (type) {
		case RPLOT_3D_NORMAL:
			/* just use typical camera values */
			mat4f_perspective(radiansf(45), 16.0/9, 0.1, 100, &p->perspective);
			break;

		case RPLOT_3D_ORTHOGONAL:
			va_start(valist, type);
			for (uint8_t i=0; i<6; i++) {
				orth_proj[i] = va_arg(valist, double);
			}
			va_end(valist);
			mat4f_ortho(orth_proj[0], 
									orth_proj[1], 
									orth_proj[2], 
									orth_proj[3], 
									orth_proj[4], 
									orth_proj[5], 
									&p->perspective);		
			break;

		case RPLOT_3D_FOLLOW:
		case RPLOT_3D_FOLLOW_HEADING:
			vec3f_copy((vec3f *)camera_pos, &p->camera_pos);
			vec3f_copy((vec3f *)camera_up, &p->camera_up);
			mat4f_perspective(radiansf(45), 16.0/9, 0.1, 100, &p->perspective);
			break;

		default:
			fprintf(stderr, "invalid type to rplot3d new\n");
			goto errout;
	}
	mat4f_mult(&p->perspective, &p->Tworld2camera, &p->Tworld2cameraProj);

	return widget;

errout:
	g_object_unref(widget);
	return NULL;
}

void 
rplot_3d_update_lines(Rplot3d *p, guint8 index, const vec3f *a, const vec3f *b)
{
	if (index < p->lines.n) {
		glBindBuffer(GL_ARRAY_BUFFER, p->lines.vbo);
		glBufferSubData(GL_ARRAY_BUFFER,
			sizeof(float)*index*7, 
			sizeof(float)*3, 
			a);
		glBufferSubData(GL_ARRAY_BUFFER, 
			sizeof(float)*(index+1)*7, 
			sizeof(float)*3, 
			b);
	}
}

void
rplot_3d_update_vehicle(Rplot3d *p, guint8 i, const vec3f *pos, const rotf *q)
{
	if (i < p->vehicles.n) {
		mat4f_rot_trans(q, pos, &p->vehicles.Tmodel2world[i]);
		if (i==0) {
			switch(p->type) {
				vec3f cam_pos; /* in world frame */

				case RPLOT_3D_FOLLOW:
					/* follow the vehicle */
					vec3f_add(&p->camera_pos, pos, &cam_pos);
					mat4f_look_at(&cam_pos, pos, &p->camera_up, &p->Tworld2camera);
					mat4f_mult(&p->perspective, &p->Tworld2camera, &p->Tworld2cameraProj);	
					break;

				case RPLOT_3D_FOLLOW_HEADING:
					mat4f_look_at(&p->camera_pos, pos, &p->camera_up, &p->Tworld2camera);
					mat4f_mult(&p->perspective, &p->Tworld2camera, &p->Tworld2cameraProj);	\
					break;

				default:
					break;
			}
		}
	}
}

void
rplot_3d_update_ellispoid(Rplot3d *p, guint8 index, const vec3f *center, const mat3f *P)
{
	if (index < p->ellipses.n) {
		if (P != NULL) {
			int i, j;
			for (i=0; i<3; i++) {
				for (j=0; j<3; j++){
					p->ellipses.Tmodel2world[index][i][j] = (*P)[i][j];
				}
			}
		}
		mat4f_translate(center, &p->ellipses.Tmodel2world[index]);
	}
}