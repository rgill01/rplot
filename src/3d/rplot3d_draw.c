/*****************************************************************************
 * GTK+ real-time plotting widget
 * Copyright (C) 2017 Rajan Gill
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#include "rplot3d_private.h"
#include <string.h>
#include <math.h>
#include <stdlib.h>

/*****************************************************************************
 * Preprocessor Definitions
 ****************************************************************************/		

static const float grid_rgba[4] = {1,1,1,0.2};
#define BACKGROUND_COLOUR       0.1,0.1,0.1,1

static const float vehicle_points[][3] =  
  {
    {0,0,0.2},
    {0,0,0},
    {-0.1,0,0},

    {0,0,0.2},
    {-0.1,0,0},
    {0,0,0},

    {0, -0.35, 0},
    {0, 0.35, 0},
    {0,0, 0.2},

    {0, -0.35, 0},
    {0,0, 0.2},
    {0, 0.35, 0},
  };

#define VEHICLE_NUM_POINTS      (sizeof(vehicle_points)/sizeof(vehicle_points[0]))

#define SPHERE_SLICES           20
#define SPHERE_INTERVAL         (2*M_PI/SPHERE_SLICES)
#define SPHERE_NUM_POINTS       (SPHERE_SLICES*(SPHERE_SLICES/2)*6)

#define SPHERE_POINTS(r,th,phi,p) \
  p[0] = r*cosf(phi)*cosf(th); \
  p[1] = r*cosf(phi)*sinf(th); \
  p[2] = r*sinf(phi); 

#define GENERATE_SPHERE_POINTS(points, r) \
  long i = 0; \
  for (float th = 0; th < 2*M_PI; th += SPHERE_INTERVAL) { \
    for (float phi = -M_PI_2; phi < M_PI_2 - SPHERE_INTERVAL; phi += SPHERE_INTERVAL) { \
      SPHERE_POINTS(r, th, phi, points[i]); \
      i++; \
      SPHERE_POINTS(r, th + SPHERE_INTERVAL, phi, points[i]); \
      i++; \
      SPHERE_POINTS(r, th + SPHERE_INTERVAL, phi + SPHERE_INTERVAL, points[i]); \
      i++; \
      SPHERE_POINTS(r, th + SPHERE_INTERVAL, phi + SPHERE_INTERVAL, points[i]); \
      i++;\
      SPHERE_POINTS(r, th, phi + SPHERE_INTERVAL, points[i]); \
      i++;\
      SPHERE_POINTS(r, th, phi, points[i]); \
      i++;\
    }\
  }

#define GRID_SIZE         50   /* +/- 50 meter in x and y at z = 0 */
#define GRID_NUM_POINTS   (GRID_SIZE*2+1)*4
#define GENERATE_GRID_POINTS(points) \
  long k = -1;\
  for (long i = -GRID_SIZE; i <= GRID_SIZE; i ++) {\
    points[++k][0] = -GRID_SIZE; \
    points[k][1] = i;\
    points[k][2] = 0;\
    points[++k][0] = GRID_SIZE;\
    points[k][1] = i;\
    points[k][2] = 0;\
    points[++k][0] = i;\
    points[k][1] = -GRID_SIZE;\
    points[k][2] = 0;\
    points[++k][0] = i;\
    points[k][1] = GRID_SIZE;\
    points[k][2] = 0; \
  }

#define GL_UPLOAD_POINTS(size, num_points, start_ptr) \
  

/*****************************************************************************
 * Private API Definitions
 ****************************************************************************/

/* Convenience function. Upload data points to GL.
 *
 */
static void gl_upload_points(
  GLuint type, long num_points, int n_obj, const float (*start)[3], const float (*rgba)[4],
  GLuint *vbo, GLuint *vao) 
{  
  glGenVertexArrays(1, vao);
  glBindVertexArray(*vao);

  glGenBuffers(1, vbo);
  glBindBuffer(GL_ARRAY_BUFFER, *vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(float)*(n_obj*num_points)*7, NULL, type);
  for (int i = 0; i < n_obj; i++) {
    for (int j = 0; j < num_points; j ++) {
      /*         [                                           ... obj 1 ....      ]
       *         [                   vertex 1         |            vertex 2      ]
       * buffer: [ pt x | pt y | pt z | r | g | b | a | pt x2 | pt y2 pt z2 | ...] 
       * note could also use multiple buffers then bind using vao.. 
       */
      glBufferSubData(GL_ARRAY_BUFFER, 
        sizeof(float)*(i*num_points+j)*7, 
        sizeof(float)*3, 
        start[j]);

      glBufferSubData(GL_ARRAY_BUFFER, 
        sizeof(float)*((i*num_points+j)*7+3),
        sizeof(float)*4, 
        rgba[i]);
    }
  } 
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float)*7, 0);
  glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(float)*7, 
    (void *) (sizeof(float)*3));
  glEnableVertexAttribArray(0);
  glEnableVertexAttribArray(1);
}

/*****************************************************************************
 * Public API Definitions
 ****************************************************************************/

void rplot_3d_draw_init(Rplot3d *p)
{
  
  /* vehicle */
  gl_upload_points(GL_STATIC_DRAW, VEHICLE_NUM_POINTS, p->vehicles.n, 
    vehicle_points, p->vehicles.rgba, &p->vehicles.vbo, &p->vehicles.vao);

  /* lines */
  static const float zeros[2][3] = {{0}};
  gl_upload_points(GL_STREAM_DRAW, 2, p->lines.n, 
    zeros, p->lines.rgba, &p->lines.vbo, &p->lines.vao);

  /* ellipsoids */
  float sphere_points[SPHERE_NUM_POINTS][3];
  GENERATE_SPHERE_POINTS(sphere_points, 1);
  gl_upload_points(GL_STATIC_DRAW, SPHERE_NUM_POINTS, p->ellipses.n,
    sphere_points, p->ellipses.rgba, &p->ellipses.vbo, &p->ellipses.vao);

  /* grid */
  float grid_points[GRID_NUM_POINTS][3];
  GENERATE_GRID_POINTS(grid_points);
  gl_upload_points(GL_STATIC_DRAW, GRID_NUM_POINTS, 1, 
    grid_points, &grid_rgba, &p->background.vbo, &p->background.vao);

  /* text */
  p->txt = glutil_text_initiate(FONT_SIZE);

  /* shader program */
  p->shader  = glutil_shaders_load(__DIR__ "/src/shaders/vs3d.glsl", 
    __DIR__ "/src/shaders/fs3d.glsl");
  p->T_ID   = glGetUniformLocation(p->shader, "T");
  glClearColor(BACKGROUND_COLOUR);
}

void rplot_3d_draw_deinit(Rplot3d *p)
{
  glDeleteProgram(p->shader);
  glutil_text_destroy(p->txt);

  glDeleteBuffers(1, &p->vehicles.vbo);
  glDeleteBuffers(1, &p->lines.vbo);
  glDeleteBuffers(1, &p->ellipses.vbo);
  glDeleteBuffers(1, &p->background.vbo);

  glDeleteVertexArrays(1, &p->vehicles.vao);
  glDeleteVertexArrays(1, &p->lines.vao);
  glDeleteVertexArrays(1, &p->ellipses.vao);
  glDeleteVertexArrays(1, &p->background.vao);
}

void rplot_3d_draw(Rplot3d *p)
{
  int i;

  glClear(GL_COLOR_BUFFER_BIT);
  /* enable blending, culling not sure yet, set shader */
  glEnable(GL_BLEND);  /* we can move these to the init, but 
                        * keep it here just in case we want to add
                        * text or other shaders in future.. 
                        */
  // glEnable(GL_CULL_FACE);
  glUseProgram(p->shader); 

  /* draw grid */
  glBindVertexArray(p->background.vao);
  glUniformMatrix4fv(p->T_ID, 1, GL_TRUE, (GLfloat *) &p->Tworld2cameraProj[0][0]);
  glDrawArrays(GL_LINES, 0, GRID_NUM_POINTS);

  /* draw lines */
  if (p->lines.n > 0) {
    glBindVertexArray(p->lines.vao);
    glDrawArrays(GL_LINES, 0, p->lines.n*2);
  }
  
  /* draw vehicles */
  glBindVertexArray(p->vehicles.vao);
  // glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
  for (i=0; i<p->vehicles.n; i ++) {
    mat4f T;
    mat4f_mult(&p->Tworld2cameraProj, &p->vehicles.Tmodel2world[i], &T);    
    glUniformMatrix4fv(p->T_ID, 1, GL_TRUE, (GLfloat *) &T[0][0]);
    glDrawArrays(GL_TRIANGLES, i*VEHICLE_NUM_POINTS, VEHICLE_NUM_POINTS);
  }

  /* draw ellisoids */
  glBindVertexArray(p->ellipses.vao);
  for (i=0; i<p->ellipses.n; i++) {
    mat4f T;
    mat4f_mult(&p->Tworld2cameraProj, &p->ellipses.Tmodel2world[i], &T);    
    glUniformMatrix4fv(p->T_ID, 1, GL_TRUE, (GLfloat *) &T[0][0]);
    glDrawArrays(GL_TRIANGLES, i*SPHERE_NUM_POINTS, SPHERE_NUM_POINTS);
  }
}