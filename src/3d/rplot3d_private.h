/*****************************************************************************
 * GTK+ real-time plotting widget
 * Copyright (C) 2017 Rajan Gill
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#ifndef __RPLOT_3D_PRIVATE_H__
#define __RPLOT_3D_PRIVATE_H__

#include <rplot3d.h>
#include <vectorf.h>
#include <glutilities.h>

/*****************************************************************************
 * Preprocessor Definitions
 ****************************************************************************/

#define FONT_SIZE					16

/*****************************************************************************
 * Private Types
 ****************************************************************************/

typedef struct 
{
	uint8_t 		n;

	GLuint 			vbo;	/* stores just one buffer of vehicle */
	GLuint 			vao;

	mat4f 			*Tmodel2world;
	float 			(*rgba)[4];
} Rplot3dVehicles;

typedef struct 
{
	uint8_t 		n;

	GLuint 			vbo;	/* stores all the lines and colours */
	GLuint 			vao;

	float 			(*rgba)[4];
} Rplot3dLines;

typedef struct 
{
	uint8_t 		n;

	GLuint 			vbo;  /* store just one buffer of sphere, then skew with T */
	GLuint 			vao;

	mat4f 			*Tmodel2world;
	float 			(*rgba)[4];
} Rplot3dEllipses;

/* todo: the above objects look the same...could consolidate and use enums or 
 * somethign to distinguish drawing types, but too lazy atm.. 
 */

typedef struct 
{
	GLuint 			vbo;
	GLuint 			vao;
} Rplot3dBackground;

struct _Rplot3d
{
	GtkGLArea 			parent;

	/* private */

	Rplot3dVehicles 	vehicles;
	Rplot3dLines    	lines;
	Rplot3dEllipses 	ellipses;
	Rplot3dBackground background;

	GLuint 				shader, T_ID;

	Rplot3dCameraType_e type;

	mat4f  				perspective, Tworld2camera, 
								Tworld2cameraProj; /* rplot draw only uses this */
	vec3f 				camera_pos, camera_up; /* save for follow mode */

	glutil_text 	*txt;
};

/*****************************************************************************
 * Public API Declarations
 ****************************************************************************/

void rplot_3d_draw_init(Rplot3d *p);
void rplot_3d_draw_deinit(Rplot3d *p);
void rplot_3d_draw(Rplot3d *p);

#endif /* include guard */