#############################################################################
# GTK+ real-time plotting widget
# Copyright (C) 2017 Rajan Gill
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#############################################################################

CC 			= gcc
AR 			= ar

CFLAGS  = `pkg-config --cflags gtk+-3.0` -Wall -Iinclude/ -I../clib -O3 \
          -D USE_GLEW \
					-D __DIR__=\"$(shell pwd)\" \
					-fPIC

LFLAGS  = `pkg-config --libs gtk+-3.0` 

LIB_LFLAGS = -L/home/rgill/GIT/clib -Wl,-rpath=/home/rgill/GIT/clib -lmyclib \
             -lGL -lGLEW
EX_LFLAGS  = -L. -Wl,-rpath=. -lrplot \
						 -lm

LOBJS 	 = $(patsubst %.c,%.o,$(wildcard src/2d/*.c) $(wildcard src/3d/*.c)) 
LIB 		 = librplot.so
EXAMPLES = $(patsubst %.c,%.out,$(wildcard ex/*.c))

INSTALL_DIR = /usr

default: $(LIB) $(EXAMPLES)

$(LIB): $(LOBJS)
	$(CC) -shared -o $@ $(LOBJS) $(LFLAGS) $(LIB_LFLAGS)

%.o: %.c
	$(CC) $(CFLAGS) -o $@ -c $<

%.out: %.o $(LIB)
	$(CC) -o $@ $< $(LFLAGS) $(EX_LFLAGS)

install: $(LIB)
	install -m 0444 $(LIB) $(INSTALL_DIR)/lib
	mkdir $(INSTALL_DIR)/include/rplot
	install -m 0444 include/*.h $(INSTALL_DIR)/include/rplot/

clean: 
	rm -f $(LOBJS) $(LIB) $(EXAMPLES)