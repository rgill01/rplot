/*****************************************************************************
 * GTK+ real-time plotting widget
 * Copyright (C) 2017 Rajan Gill
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#include <gtk/gtk.h>
#include <math.h>
#include <rplot2d.h>

gdouble t = 0;
gdouble f = 10;

static gboolean
new_data1(Rplot2d *p)
{
  rplot_2d_push_data(p, t, (const float[])
    { sin(2*M_PI*f*t), cos(t) } );
  t+=0.001;
  return TRUE;
}

static gboolean
new_data2(Rplot2d *p)
{
  rplot_2d_push_data(p, t, (const float[]){30} );
  return TRUE;
}

static gboolean
new_data3(Rplot2d *p)
{
  rplot_2d_push_data(p, t, (const float[])
    { 1/(t+1), 1/(sqrt(t)+1), atan(t) } );
  return TRUE;
}

static gboolean
render_signal(GtkGLArea *area, GdkGLContext *context, gpointer user_data)
{
  printf("tutti\n");
  return FALSE;  /* returning TRUE stops the signal from propogating back up */
}

static void
activate (GtkApplication* app,
          gpointer        user_data)
{
  GtkWidget *window;
  GtkWidget *plot1, *plot2, *plot3;
  GtkWidget *grid;

  window = gtk_application_window_new (app);
  gtk_window_set_title (GTK_WINDOW (window), "Window");
  gtk_window_set_default_size (GTK_WINDOW (window), 600 , 300);

  plot1 = rplot_2d_new(2, (const char*[]){"First", "Second"}, 10, -1, 1, 1e3, 1);
  plot2 = rplot_2d_new(1, (const char*[]){"Hello"}, 10, -20, 100, 1e3, 100);
  plot3 = rplot_2d_new(3, (const char*[]){"1", "2", "3"}, 10, -1, 1, 1e3, 100);
    
  g_timeout_add(1, (GSourceFunc) new_data1, (gpointer) plot1);
  g_timeout_add(1, (GSourceFunc) new_data2, (gpointer) plot2);
  g_timeout_add(1, (GSourceFunc) new_data3, (gpointer) plot3);

  g_signal_connect(G_OBJECT(plot1), "render" , G_CALLBACK(render_signal), NULL);

  grid = gtk_grid_new();
  // gtk_widget_set_vexpand (plot1, TRUE);
  // gtk_widget_set_vexpand (plot2, TRUE);
  // gtk_widget_set_vexpand (plot3, TRUE);
  gtk_grid_set_column_homogeneous (GTK_GRID (grid), TRUE);
  gtk_grid_set_row_homogeneous (GTK_GRID (grid), TRUE);
  
  gtk_grid_set_row_spacing( GTK_GRID(grid), 2);
  gtk_grid_attach (GTK_GRID (grid), plot1,  0,  0,  1, 1);
  gtk_grid_attach (GTK_GRID (grid), plot2, 0,   1, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), plot3, 0,  2, 1, 1);
  gtk_container_add(GTK_CONTAINER(window), grid);

  gtk_widget_show_all (window);
}

int
main (int    argc,
      char **argv)
{
  GtkApplication *app;
  int status;

  app = gtk_application_new ("org.gnu.example", G_APPLICATION_FLAGS_NONE);
  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  status = g_application_run (G_APPLICATION (app), argc, argv);
  g_object_unref (app);

  return status;
}