/*****************************************************************************
 * GTK+ real-time plotting widget
 * Copyright (C) 2017 Rajan Gill
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#include <rplot2d.h>
#include <gtk/gtk.h>
#include <math.h>

static void
activate (GtkApplication* app,
          gpointer        user_data)
{
  GtkWidget *window;
  GtkWidget *plot;

  window = gtk_application_window_new (app);
  gtk_window_set_title (GTK_WINDOW (window), "Window");
  gtk_window_set_default_size (GTK_WINDOW (window), 400 , 300);

  plot = rplot_2d_new(6, NULL, 1000,0,0,1,1);

  
  gtk_container_add(GTK_CONTAINER(window), plot);
  gtk_widget_show_all (window);


  for (gint i = 0; i < 1000; i ++)
  {
    rplot_2d_push_data(RPLOT_2D(plot), i, 
      (const float[])
      {sin(i/100.0), 2*sin(i/100.0), 0.5*sin(i/100.0-250), log(i+1), sqrt(i),
       -2 * ( i % 100 == 0 ? 1 : NAN ), i/500.0} );
  }
}

int
main (int    argc,
      char **argv)
{
  GtkApplication *app;
  int status;

  app = gtk_application_new ("org.gnu.example", G_APPLICATION_FLAGS_NONE);
  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  status = g_application_run (G_APPLICATION (app), argc, argv);
  g_object_unref (app);

  return status;
}