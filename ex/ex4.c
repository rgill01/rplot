/*****************************************************************************
 * GTK+ real-time plotting widget
 * Copyright (C) 2017 Rajan Gill
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#include <rplot3d.h>
#include <gtk/gtk.h>
#include <math.h>

gdouble t = 0;

GtkWidget *plot[3];
static gboolean
new_data(Rplot3d* p[3])
{
  vec3f pos = {cosf(t), sinf(t), 1};

  rotf q;
  float yaw   = t + M_PI_2,
        pitch = radiansf(90),
        roll  = radiansf(0);

  rotf_from_ypr(yaw, pitch, roll, &q);

  for (int i=0;i<3;i++) {
    rplot_3d_update_vehicle(p[i], 0, &pos, &q);
    rplot_3d_update_lines(p[i], 0, &vec3f_zero, &pos);
  }

  t+= 0.01;
  return TRUE;
}

static void
activate (GtkApplication* app,
          gpointer        user_data)
{
  GtkWidget *window;
  GtkWidget *grid;

  window = gtk_application_window_new (app);
  gtk_window_set_title (GTK_WINDOW (window), "Window");
  gtk_window_set_default_size (GTK_WINDOW (window), 400 , 300);

  plot[0] = rplot_3d_new(2, (const float[][4]) {{1,0,0,0.5}, {0,1,0,0.5}}, 
                      1, (const float[][4]) {{1,1,0,1}},
                      1, (const float[][4]) {{1,0,1,0.3}},
                      (vec3f) {0,-10,2},
                      (vec3f) {0,0,1},
                      RPLOT_3D_NORMAL);

  plot[1] = rplot_3d_new(2, (const float[][4]) {{1,0,0,0.5}, {0,1,0,0.5}}, 
                      1, (const float[][4]) {{1,1,0,1}},
                      1, (const float[][4]) {{1,0,1,0.3}},
                      (vec3f) {1,1,1},
                      (vec3f) {0,0,1},
                      RPLOT_3D_FOLLOW);

  plot[2] = rplot_3d_new(2, (const float[][4]) {{1,0,0,0.5}, {0,1,0,0.5}}, 
                      1, (const float[][4]) {{1,1,0,1}},
                      1, (const float[][4]) {{1,0,1,0.3}},
                      (vec3f) {0,0,10},
                      (vec3f) {0,1,0},
                      RPLOT_3D_ORTHOGONAL,
                      -10.f,10.f,-10.f,10.f,0.f,10.f);
  for (int i=0;i<3;i++)
    rplot_3d_update_vehicle(RPLOT_3D(plot[i]), 1, &vec3f_k, &rotf_identity);

  grid = gtk_grid_new();
  gtk_grid_set_column_homogeneous (GTK_GRID (grid), TRUE);
  gtk_grid_set_row_homogeneous (GTK_GRID (grid), TRUE);
  
  gtk_grid_set_row_spacing( GTK_GRID(grid), 2);
  gtk_grid_attach (GTK_GRID (grid), plot[0], 0,  0,  1, 1);
  gtk_grid_attach (GTK_GRID (grid), plot[1], 0,  1,  1, 1);
  gtk_grid_attach (GTK_GRID (grid), plot[2], 0,  2,  1, 1);
  gtk_container_add(GTK_CONTAINER(window), grid);


  gtk_widget_show_all (window);

  g_timeout_add(10, (GSourceFunc) new_data, plot);
}

int
main (int    argc,
      char **argv)
{
  GtkApplication *app;
  int status;

  app = gtk_application_new ("rplot.example", G_APPLICATION_FLAGS_NONE);
  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  status = g_application_run (G_APPLICATION (app), argc, argv);
  g_object_unref (app);

  return status;
}